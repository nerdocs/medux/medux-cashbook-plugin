from django.urls import path, include

from . import views

app_name = "cashbook"

entry_patterns = [
    path("", views.CashbookEntryList.as_view(), name="index"),
    path("add/", views.CashbookEntryCreateView.as_view(), name="add"),
]

urlpatterns = [
    path(
        "entry/", include((entry_patterns, "entry"), namespace="entry  "), name="entry"
    ),
]
