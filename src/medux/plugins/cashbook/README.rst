Cashbook Plugin
================================

This is a newly created plugin for Medux.
Please add some documentation here. 

General
-------

Medux modules are living in the ``/home/christian/Projekte/medux/medux/plugins`` setuptools entrypoint group.
They are normal Django apps, but found and loaded dynamically during startup.
As Django apps, they can have everything a "static" app also has:

PluginConfig = AppConfig
^^^^^^^^^^^^^^^^^^^^^^^^

The app configuration which should be declared in the module's ``apps.py``.

Migrations
^^^^^^^^^^

The *migrations* directory is scanned during startup, and missing
migrations should be done using ``./manage.py migrate``

Models
^^^^^^

Create your models as usual in ``models.py``, they will be included. Don't forget to run ``makemigrations`` and ``migrate`` afterwords.
