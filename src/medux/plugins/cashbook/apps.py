from django.utils.translation import gettext_lazy as _

from . import __version__
from medux.common.api import MeduxPluginAppConfig


class CashbookConfig(MeduxPluginAppConfig):
    """A GDAPS Django app plugin.

    It needs a special parameter named ``PluginMeta``. It is the key for GDAPS
    to recognize this app as a GDAPS plugin.
    ``PluginMeta`` must point to a class that implements certain attributes
    and methods.
    """

    class PluginMeta:
        """This configuration is the introspection data for plugins."""

        # the plugin machine "name" is taken from the AppConfig, so no name here
        verbose_name = _("Cashbook")
        author = "Christian González"
        author_email = "christian.gonzalez@nerdocs.at"
        vendor = "nerdocs"
        description = _("A simple Cashbook plugin")
        category = _("Base")
        visible = True
        version = __version__
        # compatibility = "medux.core>=2.3.0"

    name = "medux.plugins.cashbook"
    groups_permissions = {
        "Cashbook": {
            "cashbook.Entry": ["view", "add"],
            "cashbook.VatRate": ["view"],
        },
        "Cashbook admins": {
            "cashbook.VatRate": ["view", "add", "delete"],
        },
    }

    def ready(self):
        # This function is called after the app and all models are loaded.
        #
        # You can do some initialization here, but beware: it should rather
        # return fast, as it is called at each Django start, even on
        # management commands (makemigrations/migrate etc.).
        #
        # Avoid interacting with the database especially 'save' operations,
        # if you don't *really* have to."""

        try:
            from . import signals  # noqa: F401
        except ImportError:
            pass
