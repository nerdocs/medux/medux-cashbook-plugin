from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit, HTML
from django.forms import Select
from django.utils.translation import gettext_lazy as _
from django import forms
from djmoney.forms import MoneyField

from medux.plugins.cashbook.models import Entry


class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ["amount", "comment", "vat_rate"]

    amount = MoneyField(
        # FIXME: disabling currency_widget leads to not returning amount to form
        #  see https://github.com/django-money/django-money/issues/720
        currency_widget=Select(choices=[("EUR", _("Euro"))], attrs={"hidden": True}),
        # currency_widget=Select(choices=[("EUR", _("Euro"))]),
        # currency_choices=[("EUR", _("Euro"))],
        # default_currency="EUR",
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields["amount"].widget.attrs["autofocus"] = True
        # self.fields["amount"].widget.widgets[1].attrs["readonly"] = True
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                # Column("amount"),
                # FIXME: don't hardcode EUR
                Column(AppendedText("amount", "Euro")),
                Column("comment"),
                Column("vat_rate"),
                Column(
                    HTML("<div>&nbsp;</div>"),  # placeholder for label
                    Submit("submit", _("Save"), css_class="mt-2"),
                ),
            ),
        )

    def clean_amount(self):
        amount = self.cleaned_data.get("amount")
        # if amount and amount.amount < 0:
        #     raise forms.ValidationError(_("Amount may not be negative."))
        return amount
