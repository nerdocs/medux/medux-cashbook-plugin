from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from medux.common.api.interfaces import IMenuItem


class CashbookView(IMenuItem):
    menu = "views"
    title = _("Cashbook")
    icon = "wallet"
    url = reverse("cashbook:entry:index")
    weight = 20
