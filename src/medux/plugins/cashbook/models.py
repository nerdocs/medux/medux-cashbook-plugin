from django.contrib.auth import get_user_model
from django.db import models
from django.db.transaction import atomic
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField

from medux.common.models import (
    TenantModelMixin,
    CreatedModifiedModel,
    OptionalTenantModelMixin,
)
from medux.preferences.models import ScopedPreference

User = get_user_model()


class Balance(CreatedModifiedModel):
    """Represents an account balance at a certain timestamp.

    Do NOT create a Balance yourself, this model is automatically created when
    adding an Entry."""

    amount = MoneyField(max_digits=19, decimal_places=2, default_currency="EUR")

    class Meta:
        verbose_name = _("Balance")
        verbose_name_plural = _("Balances")

    def __str__(self):
        return f"{self.amount}"


class VatRate(OptionalTenantModelMixin, models.Model):
    vat = models.DecimalField(max_digits=4, decimal_places=2, unique=True)

    def __str__(self):
        return f"{self.vat} %"

    def save(self, **kwargs):
        # TODO: check if running initial migration. if not, deny saving pk==1
        super().save(**kwargs)

    def delete(self, using=None, keep_parents=False):
        """Makes sure that if deleted vat_rate was the default one, all default_vat in
        preferences are set back to 1"""
        if self.pk == 1:
            raise PermissionError("0% VatRate cannot be deleted.")
        else:
            # get all default_vat objects (possible mult. scopes) from settings
            ScopedPreference.objects.filter(
                namespace="cashbook", key="default_vat", tenant=self.tenant
            ).update(value=1)


# def get_default_vat():
#     return ScopedPreference.get("cashbook","default_vat", "1")


class Entry(TenantModelMixin, CreatedModifiedModel):
    # TODO: make currency configurable
    amount = MoneyField(max_digits=19, decimal_places=2, default_currency="EUR")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    balance = models.ForeignKey(Balance, on_delete=models.PROTECT, editable=False)
    comment = models.CharField(max_length=255, blank=True, null=True)
    vat_rate = models.ForeignKey(VatRate, on_delete=models.PROTECT)
    # payment = models.ForeignKey(...)
    # patient = models.ForeignKey(..., null=True)

    class Meta:
        verbose_name = _("Entry")
        verbose_name_plural = _("Entries")
        ordering = ["-created"]

    @atomic
    def save(self, *args, **kwargs):
        last = Balance.objects.last()
        if last:
            last_amount = last.amount
        else:
            last_amount = 0

        # create new balance entry with same timestamp
        balance = Balance(created=self.created)
        balance.amount = last_amount + self.amount
        balance.save()
        self.balance = balance
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.amount)
