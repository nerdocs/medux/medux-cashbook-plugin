# You can place your interfaces here, to be included from dependent plugins

# from gdaps import Interface
# @Interface
# class IMySpecialCashbookInterface:
#     """Please provide a detailed documentation of the interface."""
#
#     def do_something(self):
#         """Documentation of method"""
#
#
#     Then implement this interface in another, dependent plugin:
#
#     from .api.interfaces import IMySpecialCashbookInterface
#
#     class OtherPluginClass(IMySpecialCashbookInterface):
#
#        def do_something(self):
#            """do something."""
#
