from django.contrib import admin

from .models import Balance, Entry


class BalanceAdmin(admin.ModelAdmin):
    model = Balance


class AccountActivityAdmin(admin.ModelAdmin):
    model = Entry


# admin.site.register(Balance, BalanceAdmin)
# admin.site.register(Entry, AccountActivityAdmin)
