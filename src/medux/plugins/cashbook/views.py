#  MedUX - Open Source Electronical Medical Record
#  Copyright (c) 2021  Christian González
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging

from django.db.models import Q
from django.http import HttpResponseRedirect
from django.views.generic import CreateView, ListView

from medux.common.htmx.mixins import HtmxResponseMixin

from medux.plugins.cashbook.forms import EntryForm
from medux.plugins.cashbook.models import Entry, VatRate

logger = logging.getLogger(__name__)


class CashbookMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "available_vats": VatRate.objects.filter(
                    Q(tenant_id=self.request.user.tenant.pk) | Q(tenant_id=None),
                )
            }
        )
        return context


class CashbookEntryList(CashbookMixin, ListView):
    model = Entry
    permission_required = "cashbook.add_entry"
    template_name = "cashbook/entry_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "objects": Entry.objects.filter(
                    tenant=self.request.user.tenant
                ).order_by("-created")[:100],
            }
        )
        return context


class CashbookEntryCreateView(CashbookMixin, HtmxResponseMixin, CreateView):
    model = Entry
    form_class = EntryForm
    permission_required = "cashbook.add_entry"
    template_name = "cashbook/entry_form.html"
    success_event = "cashbookentry:created"

    def get_initial(self):
        initial = super().get_initial()
        initial["vat_rate"] = VatRate.objects.get(pk=1)
        return initial

    def form_valid(self, form):
        entry = form.save(commit=False)
        entry.tenant = self.request.user.tenant
        entry.user = self.request.user
        entry.save()
        return HttpResponseRedirect(".")
        # return HttpResponseEmpty()
