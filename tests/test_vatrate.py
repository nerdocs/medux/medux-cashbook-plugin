#  MedUX - Open Source Electronical Medical Record
#  Copyright (c) 2021  Christian González
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pytest

from medux.core.models.settings import Settings
from medux.plugins.cashbook.models import VatRate

DEFAULT_VAT = "cashbook.default_vat"


@pytest.mark.django_db
def test_delete_1():
    with pytest.raises(PermissionError):
        VatRate.objects.get(pk=1).delete()


def create_vat99() -> VatRate:
    """Creates a "99 %" vat rate and returns the object"""
    vat99 = VatRate(vat=99)
    vat99.save()
    return vat99


@pytest.mark.django_db
def test_create_vatrate():
    """Create a VatRate, assert if ok"""
    vat99 = create_vat99()
    assert vat99.vat == 99


@pytest.mark.django_db
def test_reset_default_vat_settings():
    """Create a VatRate, set 2 settings (global, vendor) defaults to it, and then
    delete it. Then make sure default_vat is reset to 1."""
    vat99 = create_vat99()

    s_global = Settings(
        key=DEFAULT_VAT, value=str(vat99.id), scope=Settings.Scope.GLOBAL
    )
    s_global.save()
    s_vendor = Settings(
        key=DEFAULT_VAT, value=str(vat99.id), scope=Settings.Scope.VENDOR
    )
    s_vendor.save()

    # delete again and see if default_vat setting changes.
    vat99.delete()

    assert Settings.get(DEFAULT_VAT) == "1"
