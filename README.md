Cashbook Plugin
======================================

This plugin provides a simple cashbook for MedUX.


Install
^^^^^^^

You can install this plugin by invoking `pip install medux-cashbook` in MedUX' virtualenv.

License
^^^^^^^

This plugin is licensed under the `GNU Affero General Public License v3 or later (AGPLv3+) <https://www.gnu.org/licenses/agpl-3.0.txt>`_